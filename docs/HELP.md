# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.3/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.3/maven-plugin/reference/html/#build-image)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#using.devtools)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#appendix.configuration-metadata.annotation-processor)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#web)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#actuator)
* [Spring Cloud AWS](https://docs.awspring.io/spring-cloud-aws/docs/2.4.2/reference/html/index.html)
* [Testcontainers](https://www.testcontainers.org/)
* [LocalStack](https://localstack.cloud)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
* [Mock AWS con Localstack en Spring Boot](https://refactorizando.com/mock-aws-localstack-spring-boot/)
* [Spring Boot with AWS S3 Bucket from zero to useful](https://medium.com/javarevisited/spring-boot-with-aws-s3-bucket-from-zero-to-useful-c0895ab26aaa)
* [Spring Boot + AWS S3 Upload File](https://www.techgeeknext.com/cloud/aws/amazon-s3-springboot-upload-file-in-s3-bucket)
* [Spring Boot With AWS S3](https://nirajsonawane.github.io/2021/05/16/Spring-Boot-with-AWS-S3/)
* [Spring Boot Integration Tests With AWS Services Using LocalStack](https://rieckpil.de/test-spring-applications-using-aws-with-testcontainers-and-localstack/)
* [Testcontainers & LocalStack for Spring Boot Functional Tests](https://www.capitalone.com/tech/software-engineering/testcontainers-and-localstack-functional-testing/)


### Help
* [Understanding localstack](https://docs.localstack.cloud/localstack/)
* [Testcontainers Localstack module](https://www.testcontainers.org/modules/localstack/)
* [Testcontainers Jupiter/JUnit 5](https://www.testcontainers.org/test_framework_integration/junit_5/)
* [AWS S3 with Localstack return 500 in a Java application](https://stackoverflow.com/questions/65189248/aws-s3-with-localstack-return-500-in-a-java-application)
* [Maven Repository](https://mvnrepository.com)
* [Spring Boot integration testing with Testcontainers](https://fullstackcode.dev/2021/05/16/springboot-integration-testing-with-testcontainers/)

### Repositories
* [Spring Cloud AWS](https://github.com/awspring/spring-cloud-aws)
* [Testcontainers java](https://github.com/testcontainers/testcontainers-java)
* [LocalStack](https://github.com/localstack/localstack)
* [LocalStack Java Utils](https://github.com/localstack/localstack-java-utils)
