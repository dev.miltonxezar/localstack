package pe.miltonxezar.poc.awslocalstack.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/bills")
public class BillsController {

    @GetMapping(path = "/")
    public String getAllBills() {
        return "bills under development";
    }
    
}
