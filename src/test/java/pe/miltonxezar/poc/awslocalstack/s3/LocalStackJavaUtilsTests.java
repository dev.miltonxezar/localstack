package pe.miltonxezar.poc.awslocalstack.s3;

import cloud.localstack.Constants;
import cloud.localstack.Localstack;
import cloud.localstack.ServiceName;
import cloud.localstack.awssdkv1.TestUtils;
import cloud.localstack.docker.LocalstackDockerExtension;
import cloud.localstack.docker.annotation.LocalstackDockerProperties;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({LocalstackDockerExtension.class})
@LocalstackDockerProperties(services = {ServiceName.S3})
@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LocalStackJavaUtilsTests {

    private static final String BUCKET = "documentation";
    public static final String FILE = "doc-upload.txt";
    public static final String FILE_PATH = "src/test/resources/" + FILE;
    public static final String FILE_PREFIX = "doc-";
    public static final String DOWNLOAD_PATH = "src/test/resources/download.txt";

    @Autowired
    private AmazonS3 amazonS3;

    @DynamicPropertySource
    static void overrideConfiguration(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("cloud.aws.credentials.access-key", TestUtils.TEST_CREDENTIALS::getAWSAccessKeyId);
        dynamicPropertyRegistry.add("cloud.aws.credentials.secret-key", TestUtils.TEST_CREDENTIALS::getAWSSecretKey);
        dynamicPropertyRegistry.add("cloud.aws.s3.region", () -> Constants.DEFAULT_REGION);
        dynamicPropertyRegistry.add("cloud.aws.s3.endpoint", Localstack.INSTANCE::getEndpointS3);
    }

    @BeforeAll
    static void beforeAll() {
        TestUtils.getClientS3().createBucket(BUCKET);
    }

    @Test
    @Order(1)
    void validatingInitialConditions() {
        assertNotNull(amazonS3, "s3 is null");
        List<Bucket> buckets = amazonS3.listBuckets();
        assertNotNull(buckets, "list of buckets is null");
        assertFalse(buckets.isEmpty(), "buckets is empty");

        Bucket bucket = buckets.get(0);
        assertNotNull(bucket, "bucket is null");
        assertEquals(BUCKET, bucket.getName(), "bucket name unexpected");
    }

    @Test
    @Order(2)
    void testUpload() throws InterruptedException {
        TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(amazonS3).build();
        Upload upload = transferManager.upload(BUCKET, FILE, new File(FILE_PATH));
        upload.waitForUploadResult();

        ListObjectsV2Result listObjectsV2Result = amazonS3.listObjectsV2(BUCKET);
        assertNotNull(listObjectsV2Result, "listObjectsV2Result is null");

        List<S3ObjectSummary> s3ObjectSummaryList = listObjectsV2Result.getObjectSummaries();
        assertFalse(s3ObjectSummaryList.isEmpty(), "s3ObjectSummaryList is empty");

        S3ObjectSummary s3ObjectSummary = s3ObjectSummaryList.get(0);
        assertEquals(FILE, s3ObjectSummary.getKey(), "s3ObjectSummary key doesn't match");
    }

    @Test
    @Order(3)
    void validatingSearchByPrefix() {
        ListObjectsV2Result listObjectsV2Result = amazonS3.listObjectsV2(BUCKET, FILE_PREFIX);
        assertNotNull(listObjectsV2Result, "listObjectsV2Result is null");

        List<S3ObjectSummary> s3ObjectSummaryList = listObjectsV2Result.getObjectSummaries();
        assertFalse(s3ObjectSummaryList.isEmpty(), "s3ObjectSummaryList is empty");

        S3ObjectSummary s3ObjectSummary = s3ObjectSummaryList.get(0);
        assertEquals(FILE, s3ObjectSummary.getKey(), "s3ObjectSummary key doesn't match");
    }

    @Test
    @Order(4)
    void validatingDownload() {
        S3Object s3Object = amazonS3.getObject(BUCKET, FILE);
        assertNotNull(s3Object, "s3Object is null");

        S3ObjectInputStream s3ObjectInputStream = s3Object.getObjectContent();
        assertNotNull(s3ObjectInputStream, "s3ObjectInputStream is null");

        try {
            FileUtils.copyInputStreamToFile(s3ObjectInputStream, new File(DOWNLOAD_PATH));
            assertTrue(Files.exists(Path.of(DOWNLOAD_PATH)),"file not exists");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        assertTrue(FileUtils.deleteQuietly(new File(DOWNLOAD_PATH)), "still exists");

    }

}
