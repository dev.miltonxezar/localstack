package pe.miltonxezar.poc.awslocalstack.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

@SpringBootTest
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LocalstackTestContainersTests {

    private static final String DOCUMENTATION = "documentation";

    @Container
    static LocalStackContainer localstack = new LocalStackContainer(DockerImageName
            .parse("localstack/localstack:latest")).withServices(S3);

    @Autowired
    private AmazonS3 s3;

    @DynamicPropertySource
    static void overrideConfiguration(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("cloud.aws.credentials.access-key", localstack::getAccessKey);
        dynamicPropertyRegistry.add("cloud.aws.credentials.secret-key", localstack::getSecretKey);
        dynamicPropertyRegistry.add("cloud.aws.s3.region", () -> localstack.getRegion());
        dynamicPropertyRegistry.add("cloud.aws.s3.endpoint", () -> localstack.getEndpointOverride(S3));
    }

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        assertNotNull(localstack, "localstack is null");
        localstack.execInContainer("awslocal", "s3", "mb", "s3://" + DOCUMENTATION);
    }

    @Test
    @Order(1)
    void validatingInitialConditions() {
        assertNotNull(s3, "s3 is null");
        List<Bucket> list  = s3.listBuckets();
        assertNotNull(list, "list of buckets is null");
        assertFalse(list.isEmpty(), "list of buckets is empty");

        Bucket bucket = list.get(0);
        assertNotNull(bucket, "bucket is null");
        assertEquals(DOCUMENTATION, bucket.getName(), "bucket name unexpected");
    }

    @Test
    @Order(2)
    void validatingUpload() {
        PutObjectResult putObjectResult = s3.putObject(DOCUMENTATION, "doc-upload.txt", new File("src/test/resources/doc-upload.txt"));
        assertNotNull(putObjectResult, "putObjectResult is null");

        ObjectListing objectListing = s3.listObjects(DOCUMENTATION);
        assertNotNull(objectListing, "objectListing is null");

        List<S3ObjectSummary> s3ObjectSummaryList = objectListing.getObjectSummaries();
        assertFalse(s3ObjectSummaryList.isEmpty(), "s3ObjectSummaryList is empty");

        S3ObjectSummary s3ObjectSummary = s3ObjectSummaryList.get(0);
        assertEquals("doc-upload.txt", s3ObjectSummary.getKey(), "s3ObjectSummary key doesn't match");
    }

    @Test
    @Order(3)
    void validatingDownload() {

        S3Object s3Object = s3.getObject(DOCUMENTATION, "doc-upload.txt");
        assertNotNull(s3Object, "s3Object is null");

        S3ObjectInputStream s3ObjectInputStream = s3Object.getObjectContent();
        assertNotNull(s3ObjectInputStream, "s3ObjectInputStream is null");

        try {
            FileUtils.copyInputStreamToFile(s3ObjectInputStream, new File("src/test/resources/download.txt"));
            assertTrue(Files.exists(Path.of("src/test/resources/download.txt")),"file not exists");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        assertTrue(FileUtils.deleteQuietly(new File("src/test/resources/download.txt")), "still exists");

    }

}
